const close = document.getElementById('close');
const modal_container = document.getElementById('modal_container');

close.addEventListener('click', () => {
  modal_container.classList.remove('show');
});
