<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="Estilos.css">
    <title>Login</title>
</head>
<body>
    
    <form action="index.php" class="formulario" method="post">
        <h1>Ingrese al sistema</h1>
        <div class="cont">
        <i class="fa-solid fa-user icon"></i>
        <input type="text" name="Ntrabajador" id="" placeholder="Número de trabajador" class="inp">
        </div>
        <div class="cont">
        <i class="fa-solid fa-key icon"></i>
        <input type="password" name="password" id="" placeholder="Contraseña" class="inp">
        </div>
        <p>¿Aún no tienes cuenta?<a href="CrearCuenta.php" class="link"> Click aquí</a></p>
        <input type="submit" value="Ingresar" class="boton" name="login">
    </form>

</body>
</html>

<?php
    $NumTrab = "";
    $contrasena = "";

    $servidor = "localhost";
    $usuario = "root";
    $contraBD = "";
    
    if($_POST)
    {

        $NumTrab = (isset($_POST['Ntrabajador']))?$_POST['Ntrabajador']:"";
        $contrasena = (isset($_POST['password']))?$_POST['password']:"";

        if($NumTrab!="" && $contrasena!="")
        {
           try{
                $conexion = new PDO("mysql:host=$servidor;dbname=proyecto_fud",$usuario,$contraBD);
                $conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $sql = "select Contrasena from funcionario where Ntrabajador=". $NumTrab.";";
                $sentencia = $conexion->prepare($sql);
                $sentencia->execute();
                $resultado=$sentencia->fetchALL();
                foreach($resultado as $result)
                {
                    
                        if($result['Contrasena'] == $contrasena)
                        {
                            $url = "Inicio.php";
                            header("Location: $url");
                        }
                        else
                        {
                            echo '<p>Datos invalidos</p>';
                        }
                }
                
            }
            catch(PDOException $error){
               echo "Conexion erronea".$error;
            }
        }
        else
        {
            echo '<p>Por favor ingresa todos los datos </p>';
        }
    }
?>