<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="Estilos.css">
    <title>Cuenta</title>
    
</head>
<body>
    <form action="CrearCuenta.php" method="post" class="formulario" id="form">
        <h1>Crear cuenta</h1>
        <div class="cont">
            <i class="fa-solid fa-user icon"></i>
            <input type="text" name="Nombre" id="" class="inp" placeholder="Nombre(s):"></div>
        <div class="cont">
            <i class="fa-solid fa-user icon"></i>
            <input type="text" name="ApellidoP" id="" class="inp" placeholder="Primer apellido:"></div>
        <div class="cont">
            <i class="fa-solid fa-user icon"></i>
            <input type="text" name="ApellidoM" id="" class="inp" placeholder="Segundo apellido:"></div>
        <div class="cont">
            <i class="fa-solid fa-key icon"></i>
            <input type="password" name="password" id="" class="inp" placeholder="Contraseña:"></div>
        <div class="cont">
            <i class="fa-solid fa-key icon"></i>
            <input type="password" name="password2" id="" class="inp" placeholder="Confirmar contraseña:"></div>
        <div class="cont">
            <i class="fa-solid fa-hashtag icon"></i>
            <input type="text" name="Ntrabajador" id="" class="inp" placeholder="Número de trabajador"></div>
        <input type="submit" value="Crear cuenta" class="boton">
    </form>
</body>
</html>

<?php
    $Nombre = "";
    $ApellidoP = "";
    $ApellidoM = "";
    $NumTrab = "";
    $contrasena = "";
    $contrasena2 = "";

    $servidor = "localhost";
    $usuario = "root";
    $contraBD = "";
    
    if($_POST)
    {

        $Nombre = (isset($_POST['Nombre']))?$_POST['Nombre']:"";
        $ApellidoP = (isset($_POST['ApellidoP']))?$_POST['ApellidoP']:"";
        $ApellidoM = (isset($_POST['ApellidoM']))?$_POST['ApellidoM']:"";
        $NumTrab = (isset($_POST['Ntrabajador']))?$_POST['Ntrabajador']:"";
        $contrasena = (isset($_POST['password']))?$_POST['password']:"";
        $contrasena2 = (isset($_POST['password2']))?$_POST['password2']:"";

        if($Nombre!="" && $ApellidoP!="" && $ApellidoM!="" && $NumTrab!="" && $contrasena!="")
        {
            if($contrasena==$contrasena2)
                try{
                    $conexion = new PDO("mysql:host=$servidor;dbname=proyecto_fud",$usuario,$contraBD);
                    $conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                    $sql = "INSERT INTO `funcionario` (`Nombre`, `ApellidoP`, `ApellidoM`, `Ntrabajador`, `Contrasena`) VALUES ('".$Nombre."', '". $ApellidoP."', '". $ApellidoM."', '". $NumTrab."', '". $contrasena."');";
                    $conexion->exec($sql);
                    $url = "index.php";
                    header("Location: $url");
                }
                catch(PDOException $error){
                   echo "Conexion erronea".$error;
                }
            else
                echo '<p>La contraseña no coincide </p>';
        }
        else
        {
            echo '<p>Por favor ingresa todos los datos </p>';
        }
    }
?>