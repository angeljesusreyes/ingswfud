<!DOCTYPE html>
<html style="font-size: 16px;" lang="es"><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Regitro Nacional de Víctimas">
    <meta name="description" content="">
    <title>Formulario</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
<link rel="stylesheet" href="Casa.css" media="screen">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="EstilosF.css">
    <meta name="generator" content="Nicepage 4.17.10, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    
    
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Casa">
    <meta property="og:type" content="website">
  </head>
  <style>
        table, td{
            border: 1px solid rgb(0, 0, 0);
            width:800px; margin: 0 auto;
        }
    </style>
  <body data-home-page="Casa.html" data-home-page-title="Casa" class="u-body u-xl-mode" data-lang="es">
  
    <section class="u-align-center u-clearfix u-valign-top u-section-1" id="carousel_bbf3">
      <div class="u-expanded-width u-palette-3-base u-shape u-shape-rectangle u-shape-1">
      <br><br><br><br><br><br><br><br><br><br>
      <h2 class="u-custom-font u-font-montserrat u-text u-text-body-alt-color u-text-default u-text-1">
        <br>Regitro Nacional de Víctimas
      </h2>
      </div>
    </section>
  
    <div id="modal_container" class="modal-container show">
      <div class="modal">
        <h2>Lugar y fecha de solicitud</h2>
        <div class="cont">
          <i class="fa-solid fa-location-dot icon"></i>
        <input type="text" name="" id="" placeholder="Estado de la república" class="inp">
        </div>
        <div class="cont">
          <i class="fa-solid fa-calendar-days icon"></i>
        <input type="text" name="" id="" placeholder="Fecha DD/MM/AAAA" class="inp">
        </div>
        <p>Por favor ingrese el Estado de la república y fecha en que se realiza el registro</p>
        <button id="close">Iniciar formulario</button>
      </div>
    </div>
    <script src="app.js"></script>

    <form action="Formulario.php" method="post">
    <div>
      <h2>1.-Datos del/la solicitante</h2>
      <table>
        <tr>
          <td style="text-align:center;">La presente solicitud se realiza por:</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="radio" name="Solicitante" value="1" id="">Víctima directa,indirecta o potencial(Continua en 2)</td>
          <td><input type="radio" name="Solicitante" value="2" id="">Víctima a traves de familiar o persona de confianza *</td>
        </tr>
        <tr>
        <td><input type="radio" name="Solicitante" value="3" id="">Servidor/a público o autoridad **</td>
        <td><input type="radio" name="Solicitante" value="4" id="">Representante legal(Continuar en 2 y llenar anexo único)</td>
        </tr>
        <tr>
          <td><input type="text" name="NombreS" id="" placeholder="Nombre" class="inp"></td>
          <td><input type="text" name="ApellidoPS" id="" placeholder="Primer apellido" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="ApellidoMS" id="" placeholder="Segundo apellido" class="inp"></td>
          <td><input type="text" name="Parentesco" id="" placeholder="Parentesco/Relación afectiva *" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="Cargo" id="" placeholder="Cargo **" class="inp"></td>
          <td><input type="text" name="Dependencia" id="" placeholder="Dependencia o Institución **" class="inp"></td>
        </tr>
        </table>
    </div>

    <div>
      <h2>2.-Datos de la víctima</h2>
      <table>
        <tr>
          <td style="text-align:center;">Tipo de víctima</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="radio" name="TipoV" id="" value="1">Directa</td>
          <td><input type="radio" name="TipoV" id="" value="2">Indirecta</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoV" id="" Value="3">Potencial</td>
        </tr>
        <tr>
          <td><input type="text" name="NombreV" id="" placeholder="Nombre(s)" class="inp"></td>
          <td><input type="text" name="ApellidoPV" id="" placeholder="Primer apellido" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="ApellidoMV" id="" placeholder="Segundo apellido" class="inp"></td>
          <td><input type="text" name="FechaNacV" id="" placeholder="AAAA/MM/DD" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="NacionalidadV" id="" placeholder="Nacionalidad" class="inp"></td>
          <td><input type="text" name="Curp" id="" placeholder="CURP" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Sexo</td>
        </tr>
      </table>
      <table>
      <tr>
          <td><input type="radio" name="Sexo" id="" value="1">Hombre</td>
          <td><input type="radio" name="Sexo" id="" value="2">Mujer</td>
          <td><input type="radio" name="Sexo" id="" value="3">Otro</td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Lugar de nacimiento</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="Pais" id="" placeholder="País" class="inp"></td>
          <td><input type="text" name="EntidadF" id="" placeholder="Entidad federativa" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="Delegacion" id="" placeholder="Delegación o municipio" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Estado civil</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="radio" name="EstadoC" value="1" id="">Casado/a</td>
          <td><input type="radio" name="EstadoC" value="2" id="">Soltero/a</td>
          <td><input type="radio" name="EstadoC" value="3" id="">Divorciado/a</td>
          <td><input type="radio" name="EstadoC" value="4" id="">Viudo/a</td>
        </tr>
        <tr>
          <td><input type="radio" name="EstadoC" value="5" id="">Separado/a</td>
          <td><input type="radio" name="EstadoC" value="6" id="">Unión libre</td>
          <td><input type="radio" name="EstadoC" value="7" id="">Concubinato</td>
          <td><input type="radio" name="EstadoC" value="8" id="">Otro <input type="text" name="" id="" placeholder="Indique cual"></td>          
        </tr>
      </table>
        <table>
          <tr>
            <td style="text-align:center;">Lugar de residencia</td>
          </tr>
        </table>
      <table>
        <tr>
          <td><input type="text" name="Calle" id="" placeholder="Calle" class="inp"></td>
          <td><input type="text" name="NumExt" id="" placeholder="Num. Ext." class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="NumInt" id="" placeholder="Num. Int." class="inp"></td>
          <td><input type="text" name="Colonia" id="" placeholder="Colonia" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="CodPos" id="" placeholder="Código postal" class="inp"></td>
          <td><input type="text" name="Telefono" id="" placeholder="Telefono" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="Localidad" id="" placeholder="Localidad" class="inp"></td>
          <td><input type="text" name="DelegacionR" id="" placeholder="Delegación o municipio" class="inp"></td>
        </tr>
        </table>
        <table>
        <tr>
          <td><input type="text" name="EntidadFR" id="" placeholder="Entidad federativa" class="inp"></td>
        </tr>
      </table>
    </div>

    <div>
      <h2>3.-Relación de la vítima indirecta con la víctima directa</h2>
      En caso de ser víctima indirecta proporcione el nombre completo de la víctima directa
      <table>
        <tr>
          <td><input type="text" name="NombreRel" id="" placeholder="Nombre(s):" class="inp"></td>
          <td><input type="text" name="ApellidoPRel" id="" placeholder="Primer apellido" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="ApellidoMRel" id="" placeholder="Segundo apellido" class="inp"></td>
          <td>Relación de la víctima directa con la víctima indirecta</td>
        </tr>
      </table>
      <table>
        <tr>
        <td><input type="text" name="RelacionV" class="inp" id="" placeholder="Conteste la pregunta ¿Qué soy de la víctima directa?"></td>
        </tr>
      </table>
    </div>

    <div>
    <h2>4.-Identificación de la víctima</h2>
      <table>
        <tr>
          <td style="text-align:center;">Se deberá anexar al presente Formato, copia de la identificación de la víctima. 
      En caso de manifestar no contar con ella en este momento, la identificación deberá ser 
      remitida a la Comisión Ejecutiva de Atención a Víctimas con posterioridad.</td>
        </tr>
        </table>
      <table>
        <tr>
          <td>¿Presenta identificación? </td>
          <td><input type="radio" name="Identificacion" value="1" id="">Si  <input type="radio" name="Identificacion" value="2" id="">No</td>
          <td><input type="radio" name="TipoI" value="1" id="">Cartilla del servicio militar</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoI" value="2" id="">Pasaporte</td>
          <td><input type="radio" name="TipoI" value="3" id="">Cédula profesional</td>
          <td><input type="radio" name="TipoI" value="4" id="">Credencial de elector</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoI" value="5" id="">Credencial oficial expedida por el IMSS o ISSSTE</td>
          <td><input type="radio" name="TipoI" value="6" id="">Certificado o constancia de estudios</td>
          <td><input type="radio" name="TipoI" value="7" id="">Constancia de residencia expedida por autoridad local</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoI" value="8" id="">Tarjeta de residencia temporal</td>
          <td><input type="radio" name="TipoI" value="9" id="">Tarjeta de residencia permanente</td>
          <td><input type="radio" name="TipoI" value="10" id="">Otro documento oficial indique aquí ¿cual?<input type="text" name="" id=""></td>
        </tr>
      </table>
    </div>

    <div>
      <h2>5.-Lugar, fecha y relato de los hechos victimizantes</h2>
      <table>
        <tr>
          <td><input type="text" name="CalleS" id="" placeholder="Calle" class="inp"></td>
          <td><input type="text" name="NumExtS" id="" placeholder="Número ext." class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="NumIntS" id="" placeholder="Númmero int." class="inp"></td>
          <td><input type="text" name="ColoniaS" id="" placeholder="Colonia" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="CodPosS" id="" placeholder="Código postal" class="inp"></td>
          <td><input type="text" name="TelefonoS" id="" placeholder="Teléfono" class="inp"></td>
        </tr>       
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Fecha</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="FechaS" id="" placeholder="AAAA/MM/DD" class="inp"></td>
          <td><input type="text" name="EntidadFS" id="" placeholder="Entidad federativa" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Por favor relate las circunstancias de modo, tiempo y lugar, antes, durante y después de los hechos victimizantes.</td>
        </tr>
      </table>   
      <table>
        <tr>
          <td><textarea name="CircunstanciasS" id="" cols="100" rows="10"></textarea></td>
        </tr>
      </table>
      </div>

  <div>  
      <h2>6. Observaciones preliminares del/la servidor/a público/a que llena el formato</h2>
      <table>
        <tr>
          <td style="text-align:center;">Tipo de daño sufrido</td>
        </tr>
        </table>
        <table>
        <tr>
          <td><input type="radio" name="TipoDanoS" value="1" id="">Fisico</td>
          <td><input type="radio" name="TipoDanoS" value="2" id ="">Psicologico</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoDanoS" value="3" id="">Patrimonial</td>
          <td><input type="radio" name="TipoDanoS" value="4" id="">Sexual</td>
        </tr>
        </table>
        <table>
        <tr>
          <td><textarea name="ObservacionesS" id="" cols="92" rows="10"></textarea></td>
        </tr>
      </table>
    </div>

    <div>
      <h2>7. Autoridades que han conocido de los hechos victimizantes</h2>
      <table>
        <tr>
          <td style="text-align:center;">Investigación ministerial</td>
        </tr>
        </table>
        <table>
        <tr>
          <td>¿Denuncio ante el ministerio público? <input type="radio" name="DenuncioMP" value="1" id="">Si <input type="radio" name="DenuncioMP" value="2" id="">No</td>
          <td><input type="text" name="Fecha" id="" placeholder="Fecha: DD/MM/AAAA" class="inp"></td>
        </tr>
        <tr>
          <td>Competencia: <input type="radio" name="CompetenciaM" value="1" id="">Federal <input type="radio" name="CompetenciaM" value="2" id="">Local</td>
          <td><input type="text" name="EntidadF" id="" placeholder="Entidad federativa" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="Delito" id="" placeholder="Delito" class="inp"></td>
          <td><input type="text" name="" id="" placeholder="Agencia Ministerio Público" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="AP" id="" placeholder="A.P./C.I./A.C. *" class="inp"></td>
          <td><input type="text" name="" id="" placeholder="Estado de la Investigación" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">* A.P.=Averiguación Previa; C.I.=Carpeta de investigación; A.C.=Acta Circunstanciada</td>
        </tr>
        <tr>
          <td style="text-align:center;">Proceso judicial</td>
        </tr>
      </table>
      <table>
        <tr>
          <td> Competencia<input type="radio" name="CompetenciaJ" value="1" id=""> Federal<input type="radio" name="CompetenciaJ" value="2" id=""> Local</td>
          <td><input type="text" name="FechaPJ" id="" placeholder="Fecha de inicio del proceso judicial DD/MM/AAAA" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="EntidadFJ" id="" placeholder="Entidad federativa" class="inp"></td>
          <td><input type="text" name="DelitoJ" id="" placeholder="Delito" class="inp"></td>
        </tr>
        <tr>
          <td><input type="text" name="NumJuzgado" id="" placeholder="Número de juzgado" class="inp"></td>
          <td><input type="text" name="NumProceso" id="" placeholder="Número de proceso" class="inp"></td>
        </tr>
        </table>
        <table>
        <tr>
          <td><input type="text" name="EstadoPJ" id="" placeholder="Estado del proceso juducial" class="inp"></td>
        </tr>
        <tr>
          <td style="text-align:center;">PROCEDIMIENTOS ANTE ORGANISMOS NACIONALES E INTERNACIONALES DE DERECHOS HUMANOS</td>
        </tr>
        </table>
        <table>
        <tr>
          <td>¿Presentó queja, petición u otro tipo de solicitud ante organismo de DD. HH.?</td>
          <td><input type="radio" name="PresentoQueja" value="1" id="">Si <input type="radio" name="PresentoQueja" value="2" id="">No</td>
          <td><input type="text" name="FechaDH" id="" placeholder="DD/MM/AAAA"></td>
        </tr>
        </table>
        <table>
        <tr>
          <td>Competencia <input type="radio" name="CompetenciaDH" value="1" id="">Federal <input type="radio" name="CompetenciaDH" value="2" id="">Local <input type="radio" name="CompetenciaDH" value="3" id="">Internacional</td>
          <td><input type="text" name="" id="" placeholder="Organismo" class="inp"></Td>
        </tr>
        <tr>
          <td><input type="text" name="" id="" placeholder="Violación a DD. HH" class="inp"></td>
          <td><input type="text" name="AutoridadR" id="" placeholder="Autoridad responsable" class="inp"></td>
        </tr>
        <tr>
          <td>Tipo de solución</td>
          <td><input type="radio" name="TSolucion" value="1" id="">Recomendación <input type="radio" name="TSolucion" value="2" id="">Conciliación</td>
        </tr>
        <tr>
          <td><input type="radio" name="TSolucion" value="3" id="">Medidas precautorias</td>
          <td><input type="radio" name="TSolucion" value="4" id="">Otra <input type="text" name="" id=""></td>
        </tr>
        <tr>
          <td><input type="text" name="Folio" id="" placeholder="Folio" class="inp"></td>
          <td><input type="text" name="EstadoDH" id="" placeholder="Estado actual" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="OtraAutoridad" id="" placeholder="Otra Autoridad" class="inp"></td>
        </tr>
      </table>
    </div>
    <br><br>
    <div>
      <table>
        <tr>
          <td>El presente documento tiene la finalidad de conocer características particulares y condiciones que pudieran suponer mayor vulnerabilidad para las víctimas
en razón de su edad, género, preferencia u orientación sexual, identidad o expresión de género, pertenencia a un pueblo o comunidad indígena, condición
de discapacidad y otros para contar con información útil para brindar atención especializada</td>
        </tr>
      </table>
      <table>
        <tr>
          <td>¿Es niña/o o adolescente?<input type="radio" name="NinaAdo" value="1" id="">Si <input type="radio" name="NinaAdo" value="2" id="">No</td>
          <td><input type="text" name="Tutor" id="" placeholder="Nombre del tutor/a"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td ><input type="text" name="Datos" id="" placeholder="Datos del tutor/a" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="Contacto" id="" placeholder="Datos de contacto del tutor/a" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td>¿Eres persona adulta mayor? <input type="radio" name="EresAdultoM" value="1" id="">Si <input type="radio" name="EresAdultoM" value="2" id="">No</td>
          <td>¿Se encuentra en situación de calle? <input type="radio" name="SituacionCalle" value="1" id="">Si <input type="radio" name="SituacionCalle" value="2" id="">No</td>
        </tr>
        <tr>
          <td>¿Tiene condición de discapacidad?<input type="radio" name="Discapacidad" value="1" id="">Si <input type="radio" name="Discapacidad" value="2" id="">No</td>
          <td>Tipo <input type="radio" name="Tipo" value="1" id="">Fisica<input type="radio" name="Tipo" value="2" id="">Mental<input type="radio" name="Tipo" value="3" id="">Intelectual<input type="radio" name="Tipo" value="4" id="">Visual<input type="radio" name="Tipo" value="5" id="">Auditiva</td>
        </tr>
        <tr>
          <td>Grado de dependencia</td>
          <td><input type="radio" name="Dependencia" value="1" id="">Moderada <input type="radio" name="Dependencia" value="2" id="">Severa <input type="radio" name="Dpenencia" value="3" id="">Gran dependencia</td>
        </tr>
        <tr>
          <td>¿Es migrante? <input type="radio" name="Migrante" value="1" id="">Si <input type="radio" name="Migrante" value="2" id="">No</td>
          <td><input type="text" name="Pais" id="" placeholder="País origen" class="inp"></td>
        </tr>
        <tr>
          <td>¿Habla español? <input type="radio" name="HablaEspanol" value="1" id="">Si <input type="radio" name="HabalaEspanol" value="2" id="">No</td>
          <td>¿Requiere traductor/a? <input type="radio" name="RequiereTra" value="1" id="">Si <input type="radio" name="RequiereTra" value="2" id="">No</td>
        </tr>
        <tr>
          <td>¿Pertenece a población/comunidad indígena? <input type="radio" name="comunidadInd" value="1" id="">Si <input type="radio" name="comunidadInd" value="2" id="">No</td>
          <td><input type="text" name="Comunidad" id="" placeholder="¿A cúal?" class="inp"></td>
        </tr>
        <tr>
          <td>¿Refugiado/a? <input type="radio" name="Refugiado" value="1" id="">Si <input type="radio" name="Refugiado" value="2"  id="">No</td>
          <td>¿Es asilado/a político/a ? <input type="radio" name="AisladoPol" value="1" id="">Si <input type="radio" name="AisladoPol" value="2" id="">No</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="text" name="" id="" placeholder="¿Ha iniciado algún trámite para obtener esta condición?" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td>¿Es defensor/a de derechos humanos? <input type="radio" name="DefensorDH" value="1" id="">Si <input type="radio" name="DefensorDH" value="2" id="">No</td>
          <td>¿Pertenece a una institución? <input type="radio" name="PerteneceInst" value="1" id="">Si <input type="radio" name="PerteneceInst" value="2" id="">No</td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Tipo de institución</td>
        </tr>
      </table>
      <table>
        <tr>
          <td><input type="radio" name="TipoInst" value="1" id="">Federal</td>
          <td><input type="radio" name="TipoInst" value="2" id="">Estatal</td>
          <td><input type="radio" name="TipoInst" value="3" id="">Sociedad civil</td>
        </tr>
        <tr>
          <td><input type="radio" name="TipoInst" value="4" id="">Asistencia privada</td>
          <td><input type="radio" name="TipoInst" value="5" id="">Religiosa</td>
          <td><input type="radio" name="TipoInst" value="6" id="">Internacional</td>
        </tr>
      </table>
      <table>
        <tr>
          <td>¿Es periodista? <input type="radio" name="Periodista" value="1" id="">Si <input type="radio" name="Periodista" value="2" id="">No</td>
          <td><input type="text" name="TipoMedio" id="" placeholder="Tipo de medio informativo" class="inp"></td>
        </tr>
        <tr>
          <td>¿Nombre del medio informativo?<input type="radio" name="" id="">Si <input type="radio" name="" id="">No</td>
          <td>¿Fue desplazado/a dentro del país o estado por condiciones de violencia?<input type="radio" name="" id="">Si <input type="radio" name="" id="">No</td>
        </tr>
        <tr>
          <td><input type="text" name="" id="" placeholder="Entidad de salida" class="inp"></td>
          <td><input type="text" name="" id="" placeholder="Entidad receptora" class="inp"></td>
        </tr>
      </table>
      <table>
        <tr>
          <td>Considera que el hecho victimizante se debió a: </td>
          <td><input type="radio" name="Motivo" value="1" id="">Religión o creencias </td>
          <td><input type="radio" name="Motivo" value="2" id="">Preferencia u orientación sexual</td>
        </tr>
        <tr>
          <td><input type="radio" name="Motivo" value="3" id="">Identidad o expresión de género</td>
          <td><input type="radio" name="Motivo" value="4" id="">Sexo</td>
          <td><input type="radio" name="Motivo" value="5" id="">Raza</td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="text-align:center;">Información de violencia contra las mujeres</td>
        </tr>
      </table>
      <table>
          <tr>
            <td><input type="radio" name="InformacionV" id="">Psicológica</td>
            <td><input type="radio" name="InformacionV" id="">Física</td>
          </tr>
          <tr>
            <td><input type="radio" name="InformacionV" id="">Económica</td>
            <td><input type="radio" name="InformacionV" id="">Patrimonial</td>
          </tr>
          <tr>
            <td><input type="radio" name="InformacionV" id="">Obstétrica</td>
            <td><input type="radio" name="InformacionV" id="">Feminicida</td>
          </tr>
          <tr>
            <td><input type="radio" name="InformacionV" id="">Sexual</td>
            <td><input type="radio" name="InformacionV" id="">Otro</td>
          </tr>
      </table>
    </div>
    <br>
    <input type="submit" value="Registrar" class="boton">
    </form>


    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-5d47"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Texto de muestra. Haz click para seleccionar el elemento de Texto.</p>
      </div></footer>
    <section class="u-backlink u-clearfix u-grey-80">
      <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
        <span>Website Templates</span>
      </a>
      <p class="u-text">
        <span>created with</span>
      </p>
      <a class="u-link" href="" target="_blank">
        <span>Website Builder Software</span>
      </a> 
    </section>

</body>
</html>


<?php
$servidor = "localhost";
$usuario = "root";
$contraBD = "";

class persona
{
    public $nombre;
    public $apellidoP;
    public $apellidoM;

    public function getNombre()
    {
        return $this->nombre;
    }
    public function getApellidoP()
    {
        return $this->apellidoP;
    }
    public function getApellidoM()
    {
        return $this->apellidoM;
    }
    
}

class funcionario extends persona
{
    public $NumTrab;

    public function getNumTrab()
    {
        return $this->$NumTrab;
    }
}

class victima extends persona
{
    public $TipoVic;
    public $FechaNac;
    public $Sexo;
    public $Nacionalidad;
    public $Curp;
    public $Pais;
    public $EntidadFNac;
    public $EstadoC;
    public $Delegacion;
    public $Calle;
    public $Colonia;
    public $NumExt;
    public $NumInt;
    public $CodPostal;
    public $Localidad;
    public $DelegacionR;
    public $EntidadFedR;
    public $Identificacion;
    public $TipoIdent;
    public $Telefono;
    public $idSolicitud;

    public function getidSolicitud()
    {
        return $this->$idSolicitud;
    }
    public function getCurp()
    {
      return $this->Curp;
    }
}

class solicitante extends persona
{
    public $Solicitante;
    public $Parentesco;
    public $Cargo;
    public $Dependencia;
}

class relacionVic
{
    public $Nombre;
    public $apellidoP;
    public $apellidoM;
    public $relacion;
}

class solicitud
{
  public $Calle;
  public $NumExt;
  public $NumInt;
  public $Colonia;
  public $CodPostal;
  public $EntidadFed;
  public $DiaHechos;
  public $Observaciones;
  public $TipoDano;
  public $Telefono;
  public $Circunstancias;
}

class autoridades
{
  public $DenuncioMP;
  public $Fecha;
  public $CompetenciaM;
  public $EntidadF;
  public $Delito;
  public $AP;
  public $CompentenciaJ;
  public $FechaPJ;
  public $EntidadFJ;
  public $DelitoJ;
  public $NumJuzgado;
  public $NumProceso;
  public $EstadoPJ;
  public $TSolucion;
  public $Folio;
  public $EstadoDH;
  public $OtraAutoridad;
  public $PresentoQueja;
  public $FechaDH;
  public $CompentenciaDH;
  public $AutoridadR;
}

class infoExt
{
  public $NinaAdo;
  public $Tutor;
  public $Datos;
  public $Contacto;
  public $EresAdultoM;
  public $SituacionCalle;
  public $Discapacidad;
  public $Tipo;
  public $Dependencia;
  public $Migrante;
  public $Pais;
  public $HablaEspanol;
  public $RequiereTra;
  public $comunidadInd;
  public $Comunidad;
  public $Refugiado;
  public $AisladoPol;
  public $DefensorDH;
  public $PorteneceInst;
  public $TipoInst;
  public $Periodista;
  public $TipoMedio;
  public $Motivo;
}

  if($_POST)
    {
      $objSolicitante = new solicitante();
      $objSolicitante->Nombre = (isset($_POST['NombreS']))?$_POST['NombreS']:"";
      $objSolicitante->ApellidoPS = (isset($_POST['ApellidoPS']))?$_POST['ApellidoPS']:"";
      $objSolicitante->ApellidoMS = (isset($_POST['ApellidoMS']))?$_POST['ApellidoMS']:"";
      $objSolicitante->Solicitante = (isset($_POST['Solicitante']))?$_POST['Solicitante']:"";
      $objSolicitante->Parentesco = (isset($_POST['Parentesco']))?$_POST['Parentesco']:"";
      $objSolicitante->Cargo = (isset($_POST['Cargo']))?$_POST['Cargo']:"";
      $objSolicitante->Dependencia = (isset($_POST['Dependencia']))?$_POST['Dependencia']:"";


      $objVictima = new victima();
      $objVictima->TipoVic = (isset($_POST['TipoV']))?$_POST['TipoV']:"";
      $objVictima->Nombre = (isset($_POST['NombreV']))?$_POST['NombreV']:"";
      $objVictima->ApellidoP = (isset($_POST['ApellidoPV']))?$_POST['ApellidoPV']:"";
      $objVictima->ApellidoM = (isset($_POST['ApellidoMV']))?$_POST['ApellidoMV']:"";
      $objVictima->FechaNac = (isset($_POST['FechaNacV']))?$_POST['FechaNacV']:"";
      $objVictima->Nacionalidad = (isset($_POST['NacionalidadV']))?$_POST['NacionalidadV']:"";
      $objVictima->Curp = (isset($_POST['Curp']))?$_POST['Curp']:"";
      $objVictima->Sexo = (isset($_POST['Sexo']))?$_POST['Sexo']:"";
      $objVictima->Pais = (isset($_POST['Pais']))?$_POST['Pais']:"";
      $objVictima->EntidadFNac = (isset($_POST['EntidadF']))?$_POST['EntidadF']:"";
      $objVictima->Delegacion = (isset($_POST['Delegacion']))?$_POST['Delegacion']:"";
      $objVictima->EstadoC = (isset($_POST['EstadoC']))?$_POST['EstadoC']:"";
      $objVictima->Calle = (isset($_POST['Calle']))?$_POST['Calle']:"";
      $objVictima->NumExt = (isset($_POST['NumExt']))?$_POST['NumExt']:"";
      $objVictima->NumInt = (isset($_POST['NumInt']))?$_POST['NumInt']:"";
      $objVictima->Colonia = (isset($_POST['Colonia']))?$_POST['Colonia']:"";
      $objVictima->CodPostal = (isset($_POST['CodPos']))?$_POST['CodPos']:"";
      $objVictima->Telefono = (isset($_POST['Telefono']))?$_POST['Telefono']:"";
      $objVictima->Localidad = (isset($_POST['Localidad']))?$_POST['Localidad']:"";
      $objVictima->DelegacionR = (isset($_POST['DelegacionR']))?$_POST['DelegacionR']:"";
      $objVictima->EntidadFedR = (isset($_POST['EntidadFR']))?$_POST['EntidadFR']:"";
      $objVictima->Identificacion = (isset($_POST['Identificacion']))?$_POST['Identificacion']:"";
      $objVictima->TipoIdent = (isset($_POST['TipoI']))?$_POST['TipoI']:"";


      $objRelacion = new relacionVic();
      $objRelacion->Nombre = (isset($_POST['NombreRel']))?$_POST['NombreRel']:"";
      $objRelacion->apellidoP = (isset($_POST['ApellidoPRel']))?$_POST['ApellidoPRel']:"";
      $objRelacion->apellidoM = (isset($_POST['ApellidoMRel']))?$_POST['ApellidoMRel']:"";
      $objRelacion->relacion = (isset($_POST['RelacionV']))?$_POST['RelacionV']:"";

      $objSolicitud = new solicitud();
      $objSolicitud->Calle = (isset($_POST['CalleS']))?$_POST['CalleS']:"";
      $objSolicitud->NumExt = (isset($_POST['NumExtS']))?$_POST['NumExtS']:"";
      $objSolicitud->NumInt = (isset($_POST['NumIntS']))?$_POST['NumIntS']:"";
      $objSolicitud->Colonia = (isset($_POST['ColoniaS']))?$_POST['ColoniaS']:"";
      $objSolicitud->CodPostal = (isset($_POST['CodPosS']))?$_POST['CodPosS']:"";
      $objSolicitud->EntidadFed = (isset($_POST['EntidadFS']))?$_POST['EntidadFS']:"";
      $objSolicitud->DiaHechos = (isset($_POST['FechaS']))?$_POST['FechaS']:"";
      $objSolicitud->Observaciones = (isset($_POST['ObservacionesS']))?$_POST['ObservacionesS']:"";
      $objSolicitud->TipoDano = (isset($_POST['TipoDanoS']))?$_POST['TipoDanoS']:"";
      $objSolicitud->Telefono = (isset($_POST['TelefonoS']))?$_POST['TelefonoS']:"";
      $objSolicitud->Circunstancias = (isset($_POST['CircunstanciasS']))?$_POST['CircunstanciasS']:"";

      $objAutoridades = new autoridades();
      $objAutoridades->DenuncioMP = (isset($_POST['DenuncioMP']))?$_POST['DenuncioMP']:"";
      $objAutoridades->Fecha = (isset($_POST['Fecha']))?$_POST['Fecha']:"";
      $objAutoridades->CompetenciaM = (isset($_POST['CompetenciaM']))?$_POST['CompetenciaM']:"";
      $objAutoridades->EntidadF = (isset($_POST['EntidadF']))?$_POST['EntidadF']:"";
      $objAutoridades->Delito = (isset($_POST['Delito']))?$_POST['Delito']:"";
      $objAutoridades->AP = (isset($_POST['AP']))?$_POST['AP']:"";
      $objAutoridades->CompetenciaJ = (isset($_POST['CompetenciaJ']))?$_POST['CompetenciaJ']:"";
      $objAutoridades->FechaPJ = (isset($_POST['FechaPJ']))?$_POST['FechaPJ']:"";
      $objAutoridades->EntidadFJ = (isset($_POST['EntidadFJ']))?$_POST['EntidadFJ']:"";
      $objAutoridades->DelitoJ = (isset($_POST['DelitoJ']))?$_POST['DelitoJ']:"";
      $objAutoridades->NumJuzgado = (isset($_POST['NumJuzgado']))?$_POST['NumJuzgado']:"";
      $objAutoridades->NumProceso = (isset($_POST['NumProceso']))?$_POST['NumProceso']:"";
      $objAutoridades->EstadoPJ = (isset($_POST['EstadoPJ']))?$_POST['EstadoPJ']:"";
      $objAutoridades->TSolucion = (isset($_POST['TSolucion']))?$_POST['TSolucion']:"";
      $objAutoridades->Folio = (isset($_POST['Folio']))?$_POST['Folio']:"";
      $objAutoridades->EstadoDH = (isset($_POST['EstadoDH']))?$_POST['EstadoDH']:"";
      $objAutoridades->OtraAutoridad = (isset($_POST['OtraAutoridad']))?$_POST['OtraAutoridad']:"";
      $objAutoridades->PresentoQueja = (isset($_POST['PresentoQueja']))?$_POST['PresentoQueja']:"";
      $objAutoridades->FechaDH = (isset($_POST['FechaDH']))?$_POST['FechaDH']:"";
      $objAutoridades->CompetenciaDH = (isset($_POST['CompetenciaDH']))?$_POST['CompetenciaDH']:"";
      $objAutoridades->AutoridadR = (isset($_POST['AutoridadR']))?$_POST['AutoridadR']:"";

      $objInfo = new infoExt();
      $objInfo->NinaAdo = (isset($_POST['NinaAdo']))?$_POST['NinaAdo']:"";
      $objInfo->Tutor = (isset($_POST['Tutor']))?$_POST['Tutor']:"";
      $objInfo->Datos = (isset($_POST['Datos']))?$_POST['Datos']:"";
      $objInfo->Contacto = (isset($_POST['Contacto']))?$_POST['Contacto']:"";
      $objInfo->EresAdultoM = (isset($_POST['EresAdultoM']))?$_POST['EresAdultoM']:"";
      $objInfo->SituacionCalle = (isset($_POST['SituacionCalle']))?$_POST['SituacionCalle']:"";
      $objInfo->Discapacidad = (isset($_POST['Discapacidad']))?$_POST['Discapacidad']:"";
      $objInfo->Tipo = (isset($_POST['Tipo']))?$_POST['Tipo']:"";
      $objInfo->Dependencia = (isset($_POST['Dependencia']))?$_POST['Dependencia']:"";
      $objInfo->Migrante = (isset($_POST['Migrante']))?$_POST['Migrante']:"";
      $objInfo->Pais = (isset($_POST['Pais']))?$_POST['Pais']:"";
      $objInfo->HablaEspanol = (isset($_POST['HablaEspanol']))?$_POST['HablaEspanol']:"";
      $objInfo->RequiereTra = (isset($_POST['RequiereTra']))?$_POST['RequiereTra']:"";
      $objInfo->comunidadInd = (isset($_POST['comunidadInd']))?$_POST['comunidadInd']:"";
      $objInfo->Comunidad = (isset($_POST['Comunidad']))?$_POST['Comunidad']:"";
      $objInfo->Refugiado = (isset($_POST['Refugiado']))?$_POST['Refugiado']:"";
      $objInfo->AisladoPol = (isset($_POST['AisladoPol']))?$_POST['AisladoPol']:"";
      $objInfo->DefensorDH = (isset($_POST['DefensorDH']))?$_POST['DefensorDH']:"";
      $objInfo->PerteneceInst = (isset($_POST['PerteneceInst']))?$_POST['PerteneceInst']:"";
      $objInfo->TipoInst = (isset($_POST['TipoInst']))?$_POST['TipoInst']:"";
      $objInfo->Periodista = (isset($_POST['Periodista']))?$_POST['Periodista']:"";
      $objInfo->TipoMedio = (isset($_POST['TipoMedio']))?$_POST['TipoMedio']:"";
      $objInfo->Motivo = (isset($_POST['Motivo']))?$_POST['Motivo']:"";

      try{
        $conexion = new PDO("mysql:host=$servidor;dbname=proyecto_fud",$usuario,$contraBD);
        $conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        $sql = "INSERT INTO `solicitante` (`Nombre`, `ApellidoP`, `ApellidoM`, `Parentesco`, `Cargo`, `Dependencia`, `TipoSolicitante`)
          VALUES ('".$objSolicitante->Nombre."', '".$objSolicitante->ApellidoPS."', '".$objSolicitante->ApellidoMS."', '".$objSolicitante->Parentesco."', '".$objSolicitante->Cargo."','".$objSolicitante->Dependencia."','".$objSolicitante->Solicitante."');";
        $conexion->exec($sql);
        

        $sql = "select max(NumSolicitante) from solicitante;";
        $sentencia = $conexion->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchALL();
        foreach($resultado as $result)
          {
            $num = $result['max(NumSolicitante)'];
          }

        $sql = "INSERT INTO `victima` (`TipoVictima`,`Nombre`, `ApellidoP`, `ApellidoM`, `Fecha`, `Sexo`, `Nacionalidad`, `CURP`, `Pais`, `Entidad`, `Delegacion`, `EdoCivil`, `Calle`, `Colonia`, `NumExt`, `NumInt`, `CodPostal`, `Localidad`, `DelegacionR`, `EntidadFedR`, `Identificacion`, `TipoIdentif`, `NumSolicitante`)
          VALUES ('".$objVictima->TipoVic."', '".$objVictima->Nombre."', '".$objVictima->ApellidoP."', '".$objVictima->ApellidoM."', '".$objVictima->FechaNac."','".$objVictima->Sexo."','".$objVictima->Nacionalidad."','".$objVictima->Curp."','".$objVictima->Pais."','".$objVictima->EntidadFNac."','".$objVictima->Delegacion."','".$objVictima->EstadoC."','".$objVictima->Calle."','".$objVictima->Colonia."','".$objVictima->NumExt."','".$objVictima->NumInt."', '".$objVictima->CodPostal."','".$objVictima->Localidad."','".$objVictima->DelegacionR."','".$objVictima->EntidadFedR."','".$objVictima->Identificacion."','".$objVictima->TipoIdent."', '".$num."');";
        $conexion->exec($sql);

        
        $sql = "INSERT INTO `relacionvictima` (`Nombre`, `ApellidoP`, `ApellidoM`, `Relacion`, `NumSolicitante`)
          VALUES ('".$objRelacion->Nombre."', '".$objRelacion->apellidoP."', '".$objRelacion->apellidoM."','".$objRelacion->relacion."', '".$num."');";
        $conexion->exec($sql);


        $sql = "INSERT INTO `solicitud` (`CURP`, `Calle`, `NumExt`, `NumInt`, `Colonia`, `CodPostal`, `EntidadFed`, `diaHechos`, `Observaciones`, `TipoDano`, `Telefono`, `Circunstancias`)
          VALUES ('".$objVictima->Curp."', '".$objSolicitud->Calle."', '".$objSolicitud->NumExt."','".$objSolicitud->NumInt."', '".$objSolicitud->Colonia."', '".$objSolicitud->CodPostal."', '".$objSolicitud->EntidadFed."', '".$objSolicitud->DiaHechos."', '".$objSolicitud->Observaciones."', '".$objSolicitud->TipoDano."', '".$objSolicitud->Telefono."', '".$objSolicitud->Circunstancias."');";
        $conexion->exec($sql);

        $sql = "INSERT INTO `informacionext` (`CURP`, `NinaAdo`, `Tutor`, `Datos`, `Contacto`, `EresAdultoM`, `SituacionCalle`, `Discapacidad`, `Tipo`, `Dependencia`, `Migrante`, `Pais`, `HablaEspanol`, `RequiereTra`, `ComunidadInd`, `Comunidad`, `Refugiado`, `AisladoPol`, `DefensorDH`, `PerteneceInst`, `TipoInst`, `Periodista`, `TipoMedio`, `Motivo`)
          VALUES ('".$objVictima->Curp."', '".$objInfo->NinaAdo."', '".$objInfo->Tutor."', '".$objInfo->Datos."', '".$objInfo->Contacto."', '".$objInfo->EresAdultoM."', '".$objInfo->SituacionCalle."', '".$objInfo->Discapacidad."', '".$objInfo->Tipo."', '".$objInfo->Dependencia."', '".$objInfo->Migrante."', '".$objInfo->Pais."', '".$objInfo->HablaEspanol."', '".$objInfo->RequiereTra."', '".$objInfo->comunidadInd."', '".$objInfo->Comunidad."', '".$objInfo->Refugiado."', '".$objInfo->AisladoPol."', '".$objInfo->DefensorDH."', '".$objInfo->PerteneceInst."', '".$objInfo->TipoInst."', '".$objInfo->Periodista."', '".$objInfo->TipoMedio."', '".$objInfo->Motivo."');";
        $conexion->exec($sql);

        $sql = "INSERT INTO `antescedentes` (`CURP`, `DenuncioMP`, `Fecha`, `CompetenciaM`, `EntidadFed`, `Delito`, `AP`, `CompetenciaJ`, `FechaPJ`, `EntidadFedJ`, `DelitoJ`, `NumJuzgado`, `NumProceso`, `EstadoPJ`, `TSolucion`, `Folio`, `EstadoDH`, `OtraAutoridad`, `PresentoQueja`, `FechaDH`, `CompetenciaDH`, `AutoridadR`)
          VALUES ('".$objVictima->Curp."', '".$objAutoridades->DenuncioMP."', '".$objAutoridades->Fecha."','".$objAutoridades->CompetenciaM."', '".$objAutoridades->EntidadF."','".$objAutoridades->Delito."','".$objAutoridades->AP."','".$objAutoridades->CompetenciaJ."','".$objAutoridades->FechaPJ."','".$objAutoridades->EntidadFJ."','".$objAutoridades->DelitoJ."','".$objAutoridades->NumJuzgado."','".$objAutoridades->NumProceso."','".$objAutoridades->EstadoPJ."','".$objAutoridades->TSolucion."','".$objAutoridades->Folio."','".$objAutoridades->EstadoDH."','".$objAutoridades->OtraAutoridad."','".$objAutoridades->PresentoQueja."','".$objAutoridades->FechaDH."','".$objAutoridades->CompetenciaDH."','".$objAutoridades->AutoridadR."');";
        $conexion->exec($sql);
        
        
      }
      catch(PDOException $error){
        echo "Conexion erronea".$error;
      }
      
    }

?>